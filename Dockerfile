FROM debian:9 as u-boot_build

MAINTAINER Jason Kushmaul <jasonkushmaul@gmail.com>
ENV DEBIAN_FRONTEND noninteractive

COPY ./motd /etc/motd
RUN echo "cat /etc/motd" >> /etc/bash.bashrc
RUN apt-get update  && apt-get install -y apt-utils
RUN apt-get install -y sudo git build-essential libncurses5-dev gawk git libssl-dev gettext zlib1g-dev swig unzip time python python3 wget binutils-mips-linux-gnu bison
RUN mkdir -p /usr/src/u-boot
RUN apt-get install -y flex
RUN apt-get install -y  gcc-mips-linux-gnu gcc-multilib-mips-linux-gnu autoconf automake 
RUN apt-get install -y vim

ENV ARCH=mips
ENV CROSS_COMPILE=mips-linux-gnu-

WORKDIR /usr/src/u-boot/
ENTRYPOINT ["bash"]
