#!/bin/bash

docker run -it --rm \
    -u $(id -u ${USER}):$(id -g ${USER}) \
    -v ${PWD}:/usr/src/u-boot/ \
    -w /usr/src/u-boot/ \
     u-boot_build
