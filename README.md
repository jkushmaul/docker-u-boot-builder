# u-boot builder

It was difficult to get this setup and running locally so I made this.

With u-boot source as your CWD:  
`run.sh`

This will pass your CWD into the container to a shell as your current user.
You can then run:  
`make ap93_defconfig && make`
